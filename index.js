// const merge = require('deepmerge');

const styles = require('@neutrinojs/style-loader');
const FilterPlugin = require('filter-webpack-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const ChildProcessPlugin = require('child-process-webpack-plugin');

module.exports = (opts = {}) => neutrino => {
  const isProduction = process.env.NODE_ENV === 'production';

  neutrino.use(
    styles({
      test: neutrino.regexFromExtensions(['scss', 'sass', 'css']),
      loaders: [
        {
          loader: require.resolve('postcss-loader'),
          useId: 'postcss',
          options: {
            plugins: function () { // post css plugins, can be exported to postcss.config.js
              return [
                require('precss'),
                require('autoprefixer')
              ];
            }
          }
        },
        {
          loader: require.resolve('sass-loader'),
          useId: 'sass',
          options: {
            sourceMap: isProduction ? false : true,
            includePaths: [
              require('path').resolve(neutrino.options.root, 'node_modules')
            ]
          }
        }
      ],
      modules: false,
      extract: {
        enabled: true,
        loader: {},
        plugin: {
          filename: '[name].css'
        }
      }
    })
  );
  neutrino.config
    .when(!isProduction, () => {
      neutrino.config.plugin('copy-style').use(FileManagerPlugin, [
        {
          onEnd: {
            copy: [
              { source: "./dist/main.css", destination: "./patternlab/source/css/theme.css" },
              { source: "./dist/main.js", destination: "./patternlab/source/js/theme.js" }
            ]
          }
        }
      ])
      neutrino.config.watch(true);
      neutrino.config.plugin('patternlab').use(ChildProcessPlugin, [
        {
          command: 'npm run pl:serve',
          prefix: 'PATTERNLAB: ',
          once: true,
          cwd: './patternlab'
        }
      ])
    })
    .when(isProduction, () => {
      neutrino.config.plugin('template-archive').use(FileManagerPlugin, [
        {
          onEnd: {
            copy: [
              { source: "./images/*", destination: "./archive/template/images" },
              { source: "./dist/main.css", destination: "./archive/template/css/theme.css" },
              { source: "./dist/main.js", destination: "./archive/template/js/theme.js" },
              { source: "*.face", destination: "./archive/template" },
              { source: "template.conf", destination: "./archive/template" },
            ],
            archive: [
              { source: "./archive", destination: "./elate-template.zip" }
            ],
            delete: [
              "./archive"
            ]
          }
        }
      ])
    });

};
