Neutrino preset for Zoho Sites template development
===================================================

*ARCHIVED:* As I switched to [Laravel Mix](https://laravel-mix.com/), this repository is no longer maintained. If you want to take it over, please mail me at ringo AT de-smet.name to request project tranferral.

# What is Neutrino?

Sharing Webpack project settings is not an easy task. That's what [NeutrinoJS](https://neutrinojs.org/)
tries to solve. The project offers setups for several different project types. Each setup can be imported
as a regular NPM dependency together with the core Neutrino library.

# What is this preset?

This preset offers a setup for Zoho Sites template development. It currently has the following
features:

* Full support for processing and bundling custom stylesheets in the theme

It aims to include the following features in the near future:

* Full support for processing and bundling custom Javascript in the theme
* Full support for processing custom images in the theme
* Generating a boilerplate project setup for a new Zoho Sites template

# Local development setup

If you want to contribute to this project, and are using `npm link` to use this as a
dependency on your theme project, watch out for this issue regarding peer dependencies:

https://github.com/neutrinojs/neutrino/issues/1298
